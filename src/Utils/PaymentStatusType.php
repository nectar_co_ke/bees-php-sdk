<?php

namespace Bees\Php\Sdk\Utils;

abstract class PaymentStatusType
{
    const mpesa_stk_poll = 'mpesa_stk_poll';
    const mpesa_poll = 'mpesa_poll';
}