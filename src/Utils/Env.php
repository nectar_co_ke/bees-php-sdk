<?php

namespace Bees\Php\Sdk\Utils;

abstract class Env
{
    const sandbox = 'sandbox';
    const live = 'live';
}